---
layout: markdown_page
title: "GitLab Webcasts"
---

What is the webcast program at GitLab?
--------------------------------------

-   The webcast program consists of regular online events for GitLab users and community members.
-   The webcasts are recorded and distributed to GitLab users, and can be referred to in the Resource Library as part of GitLab University


Monthly Program
----------------

This program is in development. For the first month, January, we'll start with one webcast, then add another and build up to the full schedule. 


### GitLab 101 - 20 mins

-   Live demo run.
-   Similar base demo script each month. 
    -   How to use GitLab - essential demo.
    -   This month’s Q+A (commonly asked questions, recent questions from twitter) 
    -   GitLab’s products and services.
    -   How to find stuff in the GitLab; getting help, direction, participantion.
    -   Community welcome mat: How to meet other GitLab users what events we’ll be at, how about you?
-   Aimed at developers or decision makers who have signed up in the last 30 days, inviting them into the community.

### Release Party - 30 - 40 mins

-   Monthly on the 23rd or the next nearest Tuesday/Wednesday/Thursday.
-   Present highlights from the new features. 
-   Refer to any resources, docs, screencasts, etc. 
-   Guest speakers from the dev team about the new features. 
-   Highlight contributors and the MVP for that month. 
-   New contributors welcomed.
-   Q+A from audience. 


### GitLab - Expert to expert - 40 mins - 1 hour

-   Live presentation, demo or discussion on monthly in-depth learning theme. 
-   Preceded by 3 weeks of blog posts, screencasts, tutorial and an invitation which leads to the online event. 
-   Live event includes: Guest speaker / interview / presentation / demo as appropriate to topic.
-   Q+A from audience. Survey to GitLab users on the topic if appropriate. 
-   After event: Blog post of findings from the Q+A, results of survey.
-   Roll-up content into an downloadable ebook, course or other way to make the content more easily accessed and reviewed.