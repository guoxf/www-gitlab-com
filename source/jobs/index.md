---
layout: markdown_page
title: "Jobs"
---

## About GitLab

GitLab is quickly becoming the standard for source code version
management for any company, large or small. At GitLab, we help large
enterprises move to Git and build better software.

## Why work for GitLab?

GitLab is growing fast.
Working for GitLab means joining a very productive, ambitious team, where independence and flexibility are both valued and required.
Your work will directly have a large impact on the present and future of GitLab. We like to spend our time on things that matter.
We are a [remote first company](https://about.gitlab.com/2015/04/08/the-remote-manifesto/) and you can work from wherever you want.
For more background please see our [about page](https://about.gitlab.com/about/), our [culture page](https://about.gitlab.com/culture/), and our [handbook](https://about.gitlab.com/handbook/).

If you see yourself as a good fit with our company’s goals and team, then please review the current job openings on this page, and submit your resume and cover letter to be considered!

<div style="text-align: center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/UTyXGx965Os" frameborder="0" allowfullscreen></iframe>
</div>

## Available Openings

*Note: we do not accept solicitation by recruiters, recruiting agencies and outsourcing organizations.* <a id="note"></a>


### Developer Evangelist

* [Description](https://about.gitlab.com/jobs/developer-evangelist/)
* [Apply](https://gitlab.workable.com/jobs/128446/candidates/new)

### Service Engineer

* [Description](https://about.gitlab.com/jobs/service-engineer/)
* [Apply](https://gitlab.workable.com/jobs/87722/candidates/new)

### Account Manager

* [Description](https://about.gitlab.com/jobs/account-manager/)
* [Apply](https://gitlab.workable.com/jobs/88120/candidates/new)

### Marketing Associate

* [Description](https://about.gitlab.com/jobs/marketing-associate/)
* [Apply](https://gitlab.workable.com/jobs/176839/candidates/new)

### Senior Account Manager

* [Description](https://about.gitlab.com/jobs/account-manager/)
* [Apply](https://gitlab.workable.com/jobs/88117/candidates/new)
