---
layout: markdown_page
title: Glossary and Terminology
---

## What is the Glossary

This contains a simplified list and definitions of some of the terms that you will encounter in your day to day activities when working at GitLab.
Please add any terms that you discover that you think would be useful for others

### 2FA

User authentication by combination of 2 different steps during login. This allows for more security.

### Access Levels

Process of selective restriction to create, view, modify or delete a resource based on a set of assigned permissions.
See, [GitLab's Permission Guidelines](http://doc.gitlab.com/ce/permissions/permissions.html)

### Active Directory (AD)

A Microsoft based directory service for windows domain networks. It uses LDAP technology under the hood

### Agile

Building and delivering software in phases/parts rather than trying to build everything at once then delivering to the user/client. The later is known as a WaterFall model

### Artifactory

*** Needs definition here

### Artifacts

*** Needs definition here

### Atlassian

A group of companies that develops software products for developers and project managers including Bitbucket, Jira, Hipchat, Confluence, Bamboo. See [Atlassian] (https://www.atlassian.com)

### Audit Log

*** Needs definition here

### Auto Defined User Group

*** Needs definition here

### Bamboo

Atlassian's CI tool similar to Gitlab CI and Jenkins

### Basic Subscription

Entry level subscription for GitLab EE currently available in packs of 10 see [Basic subscription](https://about.gitlab.com/pricing/)

### Bitbucket 

Atlassian's web hosting service for Git and Mercurial Projects i.e. GitLab.com competitor

### Branch

A branch is a parallel vesrion of a repository. Allows you to work on the repository without you affrecting the "master" branch. Alllows you to make changes without affecting the current "live"version. Whne you have made all your changes to your branch you can then merge to the master and to make the changes fo "live".

### Branded Login

Having your own logo on your GitLab instance login page instead of the GitLab logo

### CEPH

*** Needs definition here

### Code Review

Examination of a progam's code. The main aim is to maintain high standards quality of code that is being shipped.

### Code Snippet

*** Needs definition here

### Collaborator

*** Needs definition here

### Commit

*** Needs definition here

### Community

Everyone who is using GitLab

### Confluence

*** Attlassian product for collaboration of documents and projects

### Continuous Deployment

*** Needs definition here

### Continuous Integration

A process that involves adding new code commits to source code with the combined code being run on an automated test to ensure that the changes do not break the software

### Contributor

Term used to a person contributingto an Open Source Project

### Deploy Keys

A SSH key stored on the your server that grants access to a single GitLab repository. This is used by a GitLab runner to clone the code so that tests can be run against the checked out code.

### Developer 

For us(GitLab) this means a software developer, i.e. someone who makes software. Also one of the levels of access in our multi level approval system.

### Docker

*** Needs definition here

### Docker Image

*** Needs definition here

### Fork

*** Needs definition here

### Gerrit

*** Needs definition here

### Git Hooks

*** Needs definition here

### GitHost.io

*** Needs definition here

### GitHub

*** Needs definition here

### GitLab CE

Our free on Premise solution with >100,000 users

### GitLab CI

Our own Continuos Integration feature that is shipped with each instance

### GitLab EE

Our premium on premise solution that currently has Basic, Standard and Plus subscription packages with additional features and support.

### Gitlab.com

Our free SaaS for public and private repositories.

### Gitolite

*** Needs definition here

### Gitorious

A web based hosting service for projects using Git. It was acquired by GitLab and we discontinued the service. [Gitorious Acquisition Blog Post](https://about.gitlab.com/2015/03/03/gitlab-acquires-gitorious/)

### HA

High Availability

### Hip Chat

Atlassian's real time chat application for teams. Competitor to Slack, RocketChat and MatterMost.

### High Availability

*** Needs definition here

### Issue Tracker

*** Needs definition here

### Jenkins

An Open Source CI tool written using the Java programming language. Does the same job as GitLab CI, Bamboo, Travis CI. It is extremely popular. see [Jenkins](https://jenkins-ci.org/)

### Jira

Atlassian's project management software. i.e. a complex issue tracker. See[Jira](https://www.atlassian.com/software/jira)

### Kerberos

*** Needs definition here

### Labels

*** Needs definition here

### LDAP

Lightweight Directory Access Protocol - basically its a directory (electronic address book) with user information e.g. name, phone_number etc

### LDAP User Authentication

Allowing GitLab to sign in people from an LDAP server(i.e. Allow people whose names are on the electronic user directory server) to be able to use their LDAP accounts to login.

### LDAP Group Sync

*** Needs definition here

### Git LFS

*** Needs definition here

### Linux

An operating system like Windows or OS X. It is mostly used by software developers and on servers. 

### Maria DB

A community developed fork/variation of MySQL. MySQL is owned by Oracle.

### Master

*** Needs definition here

### Mercurial

A free distributed version control system like Git. Think of it as a competitor to Git.

### Merge

*** Needs definition here

### Meteor

A hip platform for building javascript apps.[Meteor] (https://www.meteor.com)

### Milestones

*** Needs definition here

### Mirror Repositories

*** Needs definition here

### MIT License

A type of software license.It lets people do anything with your code with proper attribution and without warranty. It is the most common license for open source applications written in Ruby on Rails. GitLab CE is issued under this license. Which means, you can download the code, modify it as you want even build a new commercial product using the underlying code and its not illegal. The only condition is that there is no form of waranty provided by GitLab so whatever happens if you use the code is your own problem.

### Mondo

*** Needs definition here

### Multi LDAP Server

*** Needs definition here

### My SQL

A relational database. Currently only supported if you are using EE. It is owned by Oracle.

### Namespace

*** Needs definition here

### Nginx

*** Needs definition here

### oAuth

*** Needs definition here

### Omnibus Packages

*** Needs definition here

### On Premise

On your own server. In GitLab, this refers to the ability to download GitLab EE/GitLab CE and host it on your own server rather than using GitLab.com

### Opensource

Software for which the original source code is freely available and may be reditributed and modified.

### Owner

This is the most powerful person on a GitLab project. He has the permissions of all the other users plus the additional permission of being able to destroy i.e. delete the project

### Perforce

*** Needs definition here

### Phabricator

*** Needs definition here

### Piwik Analytics

An open source analytics software to help you analyze web traffic. It is similar to google analytics only that google analytics is not open source and information is stored by google while in Piwik the information is stored in your own server hence fully private.

### Plus Subscription

GitLAb Premium EE subscription that includes training and dedicated Account Management and Service Engineer and complete support package [Plus subscription](https://about.gitlab.com/pricing/)

### PostgreSQL

A relational database. Taunted as the most advanced open source database.

### Protected Branches

*** Needs definition here

### Pull

*** Needs definition here

### Puppet

*** Needs definition here

### Push

*** Needs definition here

### RE Read Only

*** Needs definition here

### Rebase

*** Needs definition here

### Git Repository

*** Needs definition here

### Revision

*** Needs definition here

### RocketChat

An open source chat application for teams. Very similar to Slack only that is is open-source.

### Runners

Actual build machines/containers that run/execute tests you have specified to be run on GitLab CI

### SaaS

Software as a service. Software is hosted centrally and accessed on-demand i.e. when you want to. This refers to GitLab.com in our scenario

### SCM

*** Needs definition here

### Slack

Real time messaging app for teams. Used internally by  GitLab

*** Needs definition here

### Slave Servers

Also known as secondary servers. They help to spread the load over multiple machines, they also provide backups when the master/primary server crashes.

### Source Code

Program code as typed by a computer programmer. i.e. it has not yet been executed and translated by the computer to machine language.

### SSH Key

A unique identifier of a computer. It is used to identify computers without the need for a password. e.g. On GitLab I have added the ssh key of my laptop hence the GitLab instance knows that it can accept code pushes and pulls from only trusted machines whose 

### SSO

Single Sign On. An authentication process that allows you enter one username and password to access multiple applications.

### Standard Subscription

Our mid range EE subscription that includes 24/7 support, support for High Availability [Standard Subscription](https://about.gitlab.com/pricing/)

### Stash

Atlassian's Git On-Premises solution. Think of it as Atlassian's GitLab EE. It is now known as BitBucket Server.

### Subversion

*** Needs definition here

### Sudo

A program that allows you to perform superuser/administrator actions on Unix Operating Systems e.g. Linux, OS X.It actually stands for 'superuser do'

*** Needs definition here

### SVN

Abbreviation for Subversion.

### Tag

*** Needs definition here

### Tool Stack

*** Needs definition here

### Trac

An Open Source project management and bug tracking web application.

### User

Anyone interacting with the software. 

### VCS

Version Control Software

### Waterfall

A model of building software that involves collecting all requirements from the customer, then building and refining all the requirements and finally delivering the COMPLETE software to the customer that meets all the requirements specified by the customer

### Web Hooks

A way for for an app to provide other applications with real-time information. e.g. send a message to a slack channel when a commit is pushed

### Wiki

A website/system that allows for collaborative editing of its content by the users. In programming, they usually contain documentation of how to use the software




